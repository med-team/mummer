Source: mummer
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               texlive-latex-base,
               texlive-latex-recommended,
               texlive-fonts-recommended,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/mummer
Vcs-Git: https://salsa.debian.org/med-team/mummer.git
Homepage: https://mummer.sourceforge.net/
Rules-Requires-Root: no

Package: mummer
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${perl:Depends},
         gawk
Description: Efficient sequence alignment of full genomes
 MUMmer is a system for rapidly aligning entire genomes, whether
 in complete or draft form. For example, MUMmer 3.0 can find all
 20-basepair or longer exact matches between a pair of 5-megabase genomes
 in 13.7 seconds, using 78 MB of memory, on a 2.4 GHz Linux desktop
 computer. MUMmer can also align incomplete genomes; it handles the 100s
 or 1000s of contigs from a shotgun sequencing project with ease, and
 will align them to another set of contigs or a genome using the NUCmer
 program included with the system. If the species are too divergent for
 DNA sequence alignment to detect similarity, then the PROmer program
 can generate alignments based upon the six-frame translations of both
 input sequences.

Package: mummer-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Documentation for MUMmer
 MUMmer is a system for rapidly aligning entire genomes, whether
 in complete or draft form. For example, MUMmer 3.0 can find all
 20-basepair or longer exact matches between a pair of 5-megabase genomes
 in 13.7 seconds, using 78 MB of memory, on a 2.4 GHz Linux desktop
 computer. MUMmer can also align incomplete genomes; it handles the 100s
 or 1000s of contigs from a shotgun sequencing project with ease, and
 will align them to another set of contigs or a genome using the NUCmer
 program included with the system. If the species are too divergent for
 DNA sequence alignment to detect similarity, then the PROmer program
 can generate alignments based upon the six-frame translations of both
 input sequences.
 .
 This package contains the documentation for MUMmer, a system for rapidly
 aligning entire genomes.
